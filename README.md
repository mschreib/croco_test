Changelog:

* splitting jobcomp into
  job_setup_compile_src.sh
  and
  job_compile.sh

* split source code into subdirectories

* Rewrote Makefile system (will break AGRIF, etc.)

* Changed encoding of include/cste_bio_coastal.h to UTF-8

* Removed cross_matrix, replaced by gendeps.py
