#! /usr/bin/env python3

import sys
import os
import re


class CommonBlocks:
    def __init__(self):
        self.blockName: str = ""
        self.blockVars: list[str] = []


class FortranFile:
    """
    Which parts of the Fortran files to parse
    """
    INCLUDE = 1
    MODULE = 2
    COMMON = 4

    ALL = 0xffffffff

    def __init__(self, verbosity = 10):
        self.verbosity = 10


    def reset(self, parseMode = ALL):
        self.filePath = None
        self.fileBasename = None
        self.objFilepath = None


        self.parseMode = parseMode

        self.depsIncludeFiles : list[str] = [] if self.parseMode & self.INCLUDE else None
        self.depsUseModule : list[str] = [] if self.parseMode & self.MODULE else None
        self.providesModule : list[str] = [] if self.parseMode & self.MODULE else None
        self.commonBlocks : list[CommonBlocks] = [] if self.parseMode & self.COMMON else None

        # Original code
        self.sourceLinesOrig = []

        # Modified code for parsing
        self.sourceLinesParsing = []

        # Mapping from source for parser to original code
        self.sourceLineId_to_origLineId = []
        

    def printSource(self):
        for i in range(len(self.sourceLines)):
            print(f"{self.sourceLineId_to_origLineId[i]+1:>5}: {self.sourceLines[i]}")


    def _srcLinesRemoveCComments(self):
        """
        Remove C comments.

        Source: https://stackoverflow.com/questions/241327/remove-c-and-c-comments-using-python
        """

        def replacer(match):
            """
            Replace with same number of newline characters to keep same line numbers
            """
            s = match.group(0)
            c = s.count("\n")
            return "\n"*c

        lc = len(self.sourceLines)
        src = "\n".join(self.sourceLines)
        src = re.sub(
                #r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
                r'//.*?$|/\*.*?\*/"',
                replacer,
                src,
                flags=re.DOTALL | re.MULTILINE
            )
        self.sourceLines = src.split("\n")

        assert lc == len(self.sourceLines), "Internal error"


    def _srcLinesRemoveFComments(self):
        """
        Remove Fortran comments
        """

        for i in range(len(self.sourceLines)):

            # Skip precompiler directives
            m = re.match("^[ \t]*#", self.sourceLines[i])
            if m is not None:
                continue

            self.sourceLines[i] = re.sub(r"!.*", "", self.sourceLines[i])


    def _removeSrcLineById(self, lineId):
        """
        Remove given line from source file

        This also ensures a correct information about the actual line number in the original file
        """
        del self.sourceLines[lineId]
        del self.sourceLineId_to_origLineId[lineId]


    def _srcLinesRemoveBlankLines(self):
        """
        This function merges lines which are followed by some & symbol with the previous one
        """
        lineId = 0
        while lineId < len(self.sourceLines):
            line = self.sourceLines[lineId]

            line = line.replace(" ", "").replace("\t", "")

            # Remove empty lines
            if len(line) == 0:
                self._removeSrcLineById(lineId)
                continue

            lineId+=1


    def _srcLinesRemovePrecompiler(self):
        
        for i in range(len(self.sourceLines)):
            line = self.sourceLines[i]

            if re.match("^[ \t]*#[ \t]*ifdef ", line) is not None:
                self.sourceLines[i] = ""
                continue

            if re.match("^[ \t]*#[ \t]*else", line) is not None:
                self.sourceLines[i] = ""
                continue

            if re.match("^[ \t]*#[ \t]*elif ", line) is not None:
                self.sourceLines[i] = ""
                continue

            if re.match("^[ \t]*#[ \t]*endif", line) is not None:
                self.sourceLines[i] = ""
                continue



    def _srcLinesRemoveContinuations(self):
        """
        This function merges lines which are followed by some & symbol with the previous one
        """
        lineId = 0
            
        while lineId < len(self.sourceLines):
            line = self.sourceLines[lineId]

            """
            Search for line starting with & symbol
            """
            m = re.match("^[ \t]*&(.*)$", line)
            if m is not None:
                if lineId == 0:
                    raise Exception(self.debugInfoForLine(lineId)+f"Continuation of line not possible at first line")
                
                self.sourceLines[lineId-1] += m.group(1)
                self._removeSrcLineById(lineId)

                # Continue with same line since that's the new one following it due to the deletion of the current one
                continue


            """
            Search for line ending with & symbol
            """
            m = re.match("^(.*)&[ \t]*$", line, flags=re.M)
            if m is not None:
                if len(self.sourceLines) <= lineId+1:
                    raise Exception(self.debugInfoForLine(lineId)+f"Continuation of line not possible since no line exists beyond this one")
                
                self.sourceLines[lineId] = m.group(1)+self.sourceLines[lineId+1]
                self._removeSrcLineById(lineId+1)

                # Continue with same line in case there's another & symbol
                continue

            lineId += 1


    def _cleanupFiles(self):

        # We first remove all comments to avoid including commented out include or use modules
        self._srcLinesRemoveCComments()
        self._srcLinesRemoveFComments()

        self._srcLinesRemovePrecompiler()

        # Make source code very compact by removing blank lines
        # This is also required for correct continuations
        self._srcLinesRemoveBlankLines()

        # Search for continuations with & on the following line
        self._srcLinesRemoveContinuations()

        if self.verbosity >= 1000:
            print(f"Source '{self.filePath}' after preprocessing:")
            self.printSource()


    def _loadFile(self, filepath):
        f = open(filepath, "rb")

        # We use this format since other formats like utf-8 sometimes break
        # TODO: This needs to be fixed
        #content = f.read().decode("utf-8")
        content = f.read().decode("cp437")

        # Split into lines over which we can iterate
        self.sourceLines = content.split("\n")

        # Backup
        self.sourceLinesOrig = self.sourceLines.copy()

        # Original line number
        self.sourceLineId_to_origLineId = [i for i in range(len(self.sourceLines))]


    def debugInfoForLine(self, lineId):
        origLineId = self.sourceLineId_to_origLineId[lineId]

        retstr = ""
        retstr += f"Sourcefile: {self.filePath}\n"
        retstr += f"Line Nr.: {origLineId+1}\n"
        retstr += f"Line: {self.sourceLinesOrig[origLineId]}\n"
        return retstr


    def _parseFortran(self, mode=ALL):

        # Handler for interface sections. Set to true if within interface section
        in_interface_section = False

        for line_id in range(len(self.sourceLines)):
            src_line = self.sourceLines[line_id]

            if not in_interface_section:
                m  = re.match(r"[ \t]*INTERFACE[ ]*([^, ]*)", src_line, re.IGNORECASE)
                if m is not None:
                    in_interface_section = True
                    continue

            if in_interface_section:
                m  = re.match(r"[ \t]*END INTERFACE.*", src_line, re.IGNORECASE)
                if m is not None:
                    in_interface_section = False
                    continue

            if in_interface_section:
                continue

            if self.parseMode & self.INCLUDE:
                """
                Search for "#include ..."
                """
                for m in re.finditer(r"[    ]*#include[ ]*[\"<]([^\" >]*)[\">]", src_line):
                    deps_include = m.group(1)
                    self.depsIncludeFiles += [deps_include]
                if self.verbosity >= 100:
                        print(f"{output_prefix}- deps_include: '{deps_include}'")

            if self.parseMode & self.MODULE:
                """
                Search for "USE ..."
                """
                m  = re.match(r"^[ \t]*USE[ \t]*([^, ]*)", src_line, re.IGNORECASE)
                if m is not None:
                    use_module = m.group(1).lower()
                    self.depsUseModule += [use_module]
                    if self.verbosity >= 100:
                        print(f"{output_prefix}- deps_use_module: '{use_module}'")


                """
                Search for "MODULE ..."

                Stop tracking down things if it's within an "INTERFACE ... END INTERFACE"
                """

                if not in_interface_section:
                    m  = re.match(r"[ \t]*MODULE[ ]*([^, ]*)", src_line, re.IGNORECASE)
                    if m is not None:
                        module = m.group(1).lower()
                        self.providesModule += [module]

                        if self.verbosity >= 100:
                            print(f"{self.output_prefix}- provides_module: {module}")

            if self.parseMode & self.COMMON:
                """
                Search for "COMMON ..."

                common /block1/var1,var2   /block2/var3, var5
                """
                m  = re.match(r"^[ \t]*COMMON[ \t](.*)", src_line, re.IGNORECASE)
                if m is not None:
                    commonStatements = m.group(1).lower()

                    """
                    We're not processing all blocks step-by-step
                    /block1/var1,var2   /block2/var3, var5
                    """

                    while True:
                        # Match next statement
                        m  = re.match(r"^[ \t]*/([a-z0-9_]+)/([^/]*)(.*)", commonStatements)
                        if m is None:
                            print(self.debugInfoForLine(line_id), file=sys.stderr)
                            raise Exception(f"Cannot handle COMMON statement '{src_line}' with partial match '{commonStatements}' in file {self.filePath}")
                        
                        block_name = m.group(1)
                        vars = m.group(2)
                        vars = vars.replace(" ", "")    # Replace white spaces
                        vars = vars.replace("/", ",")   # Replace / with , (separator of variables)
                        vars = vars.split(",")          # Split

                        if vars[-1] == "":
                            print(self.debugInfoForLine(line_id), file=sys.stderr)
                            raise Exception(f"Empty COMMON variable in block '{block_name}' in file '{self.filePath}")

                        cb = CommonBlocks()
                        cb.blockName = block_name
                        cb.blockVars = vars
                        self.commonBlocks += [cb]

                        # Get next statements
                        commonStatements = m.group(3)
                        commonStatements = commonStatements.replace(" ", "").replace("\t", "")

                        # Terminate if nothing is left
                        if commonStatements == "":
                            break
                    

        if in_interface_section:
            raise Exception(self.debugInfoForLine(line_id)+"Interface begin/end mismatch")

    def parse(self, filepath: str, output_prefix: str = " "*4, parseMode=ALL):
        """
        Determine dependencies:
         * What this file provides (module)
         * What this file uses (via include and use module)
        """
        self.reset(parseMode)

        self.filePath = filepath
        self.output_prefix = output_prefix
        self.parseMode = parseMode

        self.fileBasename = os.path.basename(filepath)

        """
        If it's a Fortran file, we determine the basename of the object file on which it depends
        """
        basepath, ext = os.path.splitext(filepath)

        self.objFilepath = None
        if ext.lower() in [".f90", ".f"]:
            self.objFilepath = basepath+".o"
            self.filetype = "obj"
        elif ext.lower() in [".h", ".fhh"]:
            self.filetype = "header"
        else:
            self.filetype = "source"

        if self.verbosity >= 100:
            print(" - Filepath: "+filepath)


        self._loadFile(filepath)

        self._cleanupFiles()

        self._parseFortran()


    def getInfoDict(self):
        finfo = {
                'filepath': self.filePath,
                'basename': self.fileBasename,
                'filetype': self.filetype,
                'obj_filepath': self.objFilepath,
                'deps_include': self.depsIncludeFiles,
                'deps_use_module': self.depsUseModule,
                'provides_module': self.providesModule,
                'common_statements': self.commonBlocks,
            }
        
        return finfo



class FortranFiles:
    """
    This class is a convenience handler to generate Makefile dependencies from Fortran source code
    """
    def __init__(self, verbosity = 10):

        self.verbosity = verbosity

        self.reset()


    def reset(self):
        """
        Dictionary with modules

        key: module
        value: file where to find module
        """
        self.module_to_fullpath: dict[str, str] = {}


        """
        Information about each source code
       
        key: full path to source file (hence, a unique identifier)
        value: Dictionary with further information
        """
        #self.filesDict = {}


        """
        Directly the class files
        """
        self.fortranFiles : list[FortranFile] = []


        """
        Dictionary with basenames to lookup full path

        key: basename of file
        value: full name where to find source of this file
        """
        self.basename_to_fortranFiles: dict[str, FortranFile] = {}


        """
        Dictionary with filepaths to lookup full path

        key: basename of file
        value: full name where to find source of this file
        """
        self.filepathToFortranFiles: dict[str, FortranFile] = {}


    def getFortranFile(self, fileIdentifier):
        """
        Return FortranFile handler

        fileIdentifier: basename or full pathname
        """

        # First search for full pathnames
        if fileIdentifier in self.filepathToFortranFiles:
            return self.filepathToFortranFiles[fileIdentifier]
        

        # Then, search for basenames
        if fileIdentifier in self.basename_to_fortranFiles:
            return self.basename_to_fortranFiles[fileIdentifier]
        
        raise Exception(f"File '{fileIdentifier}' not found")
        
        

    def getFortranFileByModfile(self, modfile: str):

        # Then, search for basenames
        if modfile in self.module_to_fullpath:
            return self.module_to_fullpath[modfile]
        
        raise Exception(f"Module '{modfile}' not found")

        
    def parse(self, filePaths: list[str], parseMode=FortranFile.ALL):
        self.parseMode = parseMode

        for filePath in filePaths:

            """
            Load and parse files
            """
            fortranFile = FortranFile(self.verbosity)
            fortranFile.parse(filePath, parseMode=parseMode)

            self.fortranFiles += [fortranFile]

            """
            Filepath to fullpath lookup
            """
            if fortranFile.filePath in self.filepathToFortranFiles:
                raise Exception(f"'{fortranFile.filePath}' already in lookup table")
            self.filepathToFortranFiles[fortranFile.filePath] = fortranFile

            """
            Basename to fullpath lookup
            """
            if fortranFile.fileBasename in self.basename_to_fortranFiles:
                raise Exception(f"'{fortranFile.fileBasename}' already in lookup table")
            self.basename_to_fortranFiles[fortranFile.fileBasename] = fortranFile

            """
            Module lookup
            """
            for module in fortranFile.providesModule:
                if module in self.module_to_fullpath:
                    if self.verbosity >= 10:
                        print(f"WARNING: Module '{module}' already provided by file '{self.module_to_fullpath[module]}")
                        print(f"We continue, but there might be too many dependencies")

                self.module_to_fullpath[module] = fortranFile.filePath

            if self.verbosity >= 5:
                print(fortranFile.getInfoDict())



