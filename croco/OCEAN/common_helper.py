#! /usr/bin/env python3

import sys
import os
import re

from python_file_parser import *


class commonHelper():
    """
    Track down common fields of fortran files
    """
    def __init__(self, verbosity = 10):
        self.verbosity = verbosity
        self.ffiles = FortranFiles(verbosity)

    def parse(self, files):
        self.ffiles.parse(files)

    def print(self):
        for fortranFile in self.ffiles.fortranFiles:
            print(f" - File: '{fortranFile.filePath}'")
            for c in fortranFile.commonBlocks:
                print(f"   - commonBlock: '{c.blockName}'")
                for v in c.blockVars:
                    print(f"     - var: '{v}'")


    def get_makefile_dependencies(self):
        """
        Return Makefile dependencies for all files
        """
        retval = ""

        for filepath in self.ffiles.filesDict:
            finfo = self.ffiles.filesDict[filepath]

            """
            Check if this file will be compiled. If not, skip it
            """
            if finfo['obj_filepath'] == None:
                continue

            # Output object file name to create dependency for
            obj_filepath = finfo['obj_filepath']

            retval += f"{obj_filepath}:"

            file_deps = self._track_dependencies(filepath)

            # Remove circular dependency
            if obj_filepath in file_deps:
                print(f"WARNING: Circular dependency detected for '{obj_filepath}'")
                file_deps.remove(obj_filepath)

            """
            Create dependency list
            """
            for dfile in file_deps:
                basename = os.path.basename(dfile)
                if basename in self.basename_to_fullpath:
                    retval += " "+self.basename_to_fullpath[basename]
                elif dfile.endswith(".o"):
                    retval += " "+dfile
                else:
                    if basename not in ['netcdf.inc']:
                        if self.verbosity > 0:
                            print(f"WARNING: File '{basename}' not found")

            retval += "\n"

        return retval


def print_help():
    print("""

This script converts all common fields used in header files into modules.

Usage:

"""+sys.argv[0]+""" [program options] [file1] [file2] [...]

[file1/2/3] can be Fortran files (.f90, .f, etc.) as well as header files (.fhh, .h)

Options:
    -v [int]    Specify verbosity level (0=no output, 100=output all)

If no output file is given, the output is written to stdout
""")


if len(sys.argv) <= 1:
    print_help()
    sys.exit(1)


files = []
output_file = None
verbosity = 0


"""
Poor man's argument parser
"""

# Type of argument we are parsing. Default is 'file'
state = "file"
for a in sys.argv[1:]:
    if state == "output":
        output_file = a
        state = "file"
        continue

    if state == "verbose":
        verbosity = int(a)
        state = "file"
        continue

    if a == '-o':
        state = "output"
        continue

    if a == '-v':
        state = "verbose"
        continue

    if a == '-h':
        print_help()
        sys.exit(0)

    if a[0] == "-":
        raise Exception(f"Unhandled argument '{a}'.")

    if state == "file":
        files.append(a)
        continue

    raise Exception("Internal parser error.")

if state != "file":
    raise Exception("Invalid arguments provided.")

if verbosity >= 100:
    print("Files:", files)


g = commonHelper(verbosity)
g.parse(files)

g.print()
sys.exit(1)

c = g.get_makefile_dependencies()

if output_file != None:
    f = open(output_file, "w")
    f.write(c)

else:
    print("Make dependencies:")
    print(c, end="")

