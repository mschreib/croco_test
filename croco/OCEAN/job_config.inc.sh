#!/bin/bash

####################################################
#               COMPILATION JOB                    #
####################################################

# This script assumes default compilation options, to
# change those options : 
# it can either be  edited to add your own options
# or take into account the following 
# environment variables for compilation choices :
#
# CROCO_NETCDFLIB      : netcdf library
# CROCO_NETCDFINC      : netcdf include 
# CROCO_PRISM_ROOT_DIR : OASIS-MCT directory 
# CROCO_XIOS_ROOT_DIR  : XIOS directory
#
# CROCO_CFT1           : compiler
# CROCO_FFLAGS1        : compilation otpions
# CROCO_F90FLAGS1      : compilation otpions
#
# Note that environment variables overwrite hard-coded
# options

#
# set source, compilation and run directories
#
export OCEAN_SOURCE_DIR="${HOME}/croco/croco/OCEAN"
export RUN_DIR="`pwd`"
export COMPILE_DIR="${RUN_DIR}/compile_src"
export ROOT_DIR="$OCEAN_SOURCE_DIR/.."
export LOCAL_SOURCE_DIR="${RUN_DIR}/src"

export USE_POSEIDON=0


# Activate verbose job
export CROCO_SETUP_VERBOSITY=0

#
# Default is autodetect
# Checkout make_helpers/compiler_* files and use them without .inc.sh postfix
#
export CROCO_COMPILER="compiler_autodetect"

#
# set MPI directories if needed
#
export MPIF90="mpif90"
export MPILIB=""
export MPIINC=""

#
# set NETCDF directories
#
#-----------------------------------------------------------
# Use : 
#-lnetcdf           : version netcdf-3.6.3                --
#-lnetcdff -lnetcdf : version netcdf-4.1.2                --
#-lnetcdff          : version netcdf-fortran-4.2-gfortran --
#-----------------------------------------------------------
#
#export NETCDFLIB="-L/usr/local/lib -lnetcdf"
#export NETCDFINC="-I/usr/local/include"
export NETCDFLIB=$(nf-config --flibs)
export NETCDFINC=-I$(nf-config --includedir)

#
# set OASIS-MCT (or OASIS3) directories if needed
#
export PRISM_ROOT_DIR=../../../oasis3-mct/compile_oa3-mct

#
# set XIOS directory if needed
#
# if coupling with OASIS3-MCT is activated :
# => you need to use XIOS compiled with the "--use_oasis oasis3_mct" flag
#-----------------------------------------------------------
export XIOS_ROOT_DIR=$HOME/xios

#
# Make
#
export MAKE=gmake
# Use GNU Make command, else make
which $MAKE > /dev/null 2>&1 || export MAKE=make

#
# AGRIF sources directory
#
AGRIF_SRC=${ROOT_DIR}/AGRIF


###############################################################################
# Compiler flags - only modify them if really necessary #######################
###############################################################################

#
# determine operating system
#
export OS=`uname`


function croco_print_config() {
	croco_echo "CROCO Config:"

	croco_echo_indent
	croco_echo "Source directory: $OCEAN_SOURCE_DIR"
	croco_echo "Local source directory: $LOCAL_SOURCE_DIR"
	croco_echo "Compile directory: $COMPILE_DIR"
	croco_echo "Run directory: $RUN_DIR"
	croco_echo "Root directory: $ROOT_DIR"
	croco_echo "Compiler: $CROCO_COMPILER"
	croco_echo_unindent
}

