
CPP1="cpp"
CFT1="xlf95 -I${HOME}/include/"
MAKEAGRIF="Makefile.ibm.64"
FFLAGS1="${FFLAGS1} -q64 -qwarn64 -qfixed -qrealsize=8 -qintsize=8 -qhot \
		-qalias=noaryovrlp -qthreaded -O3 -qarch=pwr4 -qtune=pwr4 -qunroll=yes"
F90FLAGS1="${F90FLAGS1} -q64 -qwarn64 -qfixed -qrealsize=8 -qintsize=8 -qhot \
		-qalias=noaryovrlp -qthreaded -O3 -qarch=pwr4 -qtune=pwr4 -qunroll=yes"
