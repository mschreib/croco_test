
#
# generates LDFLAGS1 and CPPFLAGS1
#
#
# use default NETCDF* is CROCO* is not set
LDFLAGS1="${CROCO_NETCDFLIB-$NETCDFLIB}"
CPPFLAGS1="${CROCO_NETCDFINC-$NETCDFINC}"

SCRIPT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}")" && pwd)"

#
# Try to autodetect compiler
#
if [[ "${OS}" == Linux || "${OS}" == Darwin ]]; then # Unix derivative
	COMPDET=0
	type ifort 2>/dev/null 1>&2 && COMPDET=1
	if [ $COMPDET -eq 1 ]; then
		echo "Detected ifort"
		export FC=ifort
		source "${SCRIPT_DIR}/compiler_generic_unix_ifort.inc.sh"
		return 0
	fi

	type gfortran 2>/dev/null 1>&2 && COMPDET=1
	if [ $COMPDET -eq 1 ]; then
		echo "Detected gfortran"
		source "${SCRIPT_DIR}/compiler_generic_unix_gfortran.inc.sh"
		return 0
	fi

	echo "Unable to determine compiler for unix platform"
	exit 1
fi

if [[ "${OS}" == "CYGWIN_NT-10.0" ]]; then  # ======== CYGWIN =======
	source "${SCRIPT_DIR}/compiler_generic_cygwin_gfortran.inc.sh"
	return 0
fi

if [[ "${OS}" == "AIX" ]]; then             # ===== IBM =====
	source "${SCRIPT_DIR}/compiler_generic_aix_xlf.inc.sh"
	return 0
fi

echo "Unknown Operating System '${OS}'"
exit 1

