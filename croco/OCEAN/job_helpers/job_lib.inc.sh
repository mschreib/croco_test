

#
# For nice indentation
#

CROCO_ECHO_INDENT=0
function croco_echo_indent {
	CROCO_ECHO_INDENT=$(($CROCO_ECHO_INDENT+1))
	return 0
}
function croco_echo_indentation {
	local i
	if [ 0 -lt $CROCO_ECHO_INDENT ]; then
		for i in `seq 1 $CROCO_ECHO_INDENT`; do
			echo -n "+"
		done

		echo -n " "
	fi
	return 0
}
function croco_echo_unindent {
	CROCO_ECHO_INDENT=$(($CROCO_ECHO_INDENT-1))

	# Always output trailing newline
	#echo ""
	return 0
}

function croco_echo_error {
	echo -en '\033[1;31m'
	echo -e "[ERROR] $@"
	echo -en '\033[0m'
	return 0
}

function croco_echo_raw {
	test 1 -eq $CROCO_ECHO_INDENT && echo -en '\033[7;30m'
	test 2 -eq $CROCO_ECHO_INDENT && echo -en '\033[1m'

	echo -en "$@"

	echo -en '\033[0m'
	return 0
}

function croco_echo_color_red {
	echo -en '\033[1;31m'
	return 0
}
function croco_echo_color_green {
	echo -en '\033[1;32m'
	return 0
}
function croco_echo_color_yellow {
	echo -en '\033[1;33m'
	return 0
}
function croco_echo_color_reset {
	echo -en '\033[0m'
	return 0
}

function croco_echo {
	croco_echo_indentation
	croco_echo_raw "$@\n"
	return 0
}

function croco_echo_nonl {
	croco_echo_indentation
	croco_echo_raw "$@"
	return 0
}
function croco_echo_append {
	croco_echo_raw "$@"
	return 0
}
function croco_echo_append_nl {
	croco_echo_raw "$@\n"
	return 0
}
function croco_echo_nl {
	echo ""
	return 0
}

function croco_echo_verbose_debug {
	# Comment / uncomment for verbose output
	test $CROCO_SETUP_VERBOSITY -ge 1 && croco_echo "[debug] $@"
	return 0
}



#
# Setup source files
#
# Here, we link OCEAN_SOURCE_DIR code rather than making a copy of it
#
# Link to directory which is specified as the last element
#
function setup_source_dst() {
	local ARGS=( "$@" )	# Convert to array
	# We can now iterate over this array with
	# for i in "${ARGS[@]}"; do echo " >>> $i"; done; exit

	local LEN=${#ARGS[@]}	# Number of parameters
	local SRC_ARRAY=( "${@:1:$LEN-1}" )	# All, but last argument, don't use "..." to keep array
	local DST="${ARGS[-1]}"			# Get last argument

	mkdir -p "${DST}"

	local i
	for i in "${SRC_ARRAY[@]}"; do
		test -e "$i" || return 0

		# Use force by default
		croco_echo_verbose_debug "Creating link '$i' -> '$DST'"
		ln -sf "$i" "$DST" || { echo "Error: $i $DST"; exit 1; }
	done

	return 0
}

#
# Link OCEAN_SOURCE_DIR code rather than making a copy of it
# Link to default folder
#
function setup_source() {
	setup_source_dst "$@" "$COMPILE_DIR"
	return 0
}

