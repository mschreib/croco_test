
#
# Unix system + gfortran
#

CPP1="cpp"
CPPFLAGS1="-traditional -DLinux"

CFT1="gfortran"
FFLAGS1="$FFLAGS1 -O3 -fdefault-real-8 -fdefault-double-8 -fallow-argument-mismatch -ffixed-line-length-none -ffixed-form"
F90FLAGS1="$F90FLAGS1 -O3 -fdefault-real-8 -fdefault-double-8 -fallow-argument-mismatch -ffree-line-length-none -ffree-form"

#source compiler_include_dirs_gfortran.inc.sh

# Directory for modules and include header files
CPPFLAGS1="$CPPFLAGS1 -I${COMPILE_DIR}"
FFLAGS1="$FFLAGS1 -I${COMPILE_DIR}"
F90FLAGS1="$F90FLAGS1 -I${COMPILE_DIR}"

for i in ${COMPILE_SUBDIRS}; do
	CPPFLAGS1="$CPPFLAGS1 -I${COMPILE_DIR}/${i}"
	FFLAGS1="$FFLAGS1 -I${COMPILE_DIR}/${i}"
	F90FLAGS1="$F90FLAGS1 -I${COMPILE_DIR}/${i}"
done


# Output directory for modules
FFLAGS1="$FFLAGS1 -J${COMPILE_DIR}"
F90FLAGS1="$F90FLAGS1 -J${COMPILE_DIR}"

# FFLAGS1="-O0 -g -fdefault-real-8 -fdefault-double-8 -fbacktrace \
#	-fbounds-check -finit-real=nan -finit-integer=8888"



# Get gfortran major version
MAJOR_VER=$($CFT1 -E -dM - < /dev/null | grep __GNUC__ | sed "s/.*__GNUC__[ 	]*//")

#
# We need to provide a fix for gfortran version >= 10
# Argument mismatches are checked more carefully and we need to ignore them
#
if [ $MAJOR_VER -ge 10 ]; then
	FFLAGS1="$FFLAGS1 -fallow-argument-mismatch"
	F90FLAGS1="$F90FLAGS1 -fallow-argument-mismatch"
fi
