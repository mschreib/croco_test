#! /usr/bin/env python3

import os
import sys

import argparse
from psyclone.psyir.nodes.codeblock import CodeBlock

parser = argparse.ArgumentParser(description='Poseidon')

parser.add_argument(
                    '-i',
                    dest='source_file',
                    action='store',
                    default='',
                    required=True,
                    help='input file'
                    )

parser.add_argument(
                    '-o',
                    dest='output_file',
                    action='store',
                    default='',
                    required=True,
                    help='output file'
                    )

parser.add_argument(
                    '-d',
                    dest='debug_output_file',
                    action='store',
                    default='',
                    required=False,
                    help='debug output file'
                    )

parser.add_argument(
                    '--fixed-form-output',
                    dest='fixed_form_output',
                    action='store',
                    type=bool,
                    default=False,
                    help='Use fixed form output'
                    )

parser.add_argument(
                    '-v',
                    dest='verbosity',
                    action='store',
                    default=0,
                    required=False,
                    type=int,
                    help='Verbosity'
                    )

args = parser.parse_args()




#mode = "copy"
#mode = "fparser2"

# Only tested with PSyIR
mode = "psyir"


if False:
#if True:
    """
    Determine output format
    Files ending with .f and .F or containing .f_ or .F_ should be fixed form output
    """

    fixedform_output = False
    fixedform_output |= args.output_file.endswith(".f")
    fixedform_output |= args.output_file.endswith(".F")
    fixedform_output |= args.output_file.find(".f_") != -1
    fixedform_output |= args.output_file.find(".F_") != -1

else:
    fixedform_output = args.fixed_form_output


if 0:
    """
    Special treatment

    This shouldn't be necessary anymore.
    """
    import os
    input_filename = os.path.basename(args.source_file)
    
    if mode == "fparser2":
        """
        We need to exclude this file since it doesn't work with fparser2
        """
        excluded_files = []
        # Fixed by fixing file
        #excluded_files += ['get_initial.F']
    
        excluded_files = [i.replace(".F", ".f_pass1") for i in excluded_files]
        
        if input_filename in excluded_files:
            mode = "copy"
    
    elif mode == "psyir":
        excluded_files = []

        #if fixedform_output:
        if False:
        #if True:
            """
            Exclude all GOTO statements!
            """
            excluded_files += [
                'autotiling.F',
                'bio_diag.F',
                'checkdims.F',
                'checkkwds',
                'check_kwds.F',
                'checkkwds.F',
                'check_switches1.F',
                'cppcheck.F',
                'cross_matrix.F',
                'def_bio_diags.F',
                'def_diags_eddy.F',
                'def_diags_ek.F',
                'def_diags.F',
                'def_diagsM.F',
                'def_diags_pv.F',
                'def_diags_vrt.F',
                'def_floats.F',
                'def_his.F',
                'def_rst.F',
                'def_sta.F',
                'def_surf.F',
                'diag.F',
                'fillvalue.F',
                'get_bry_bio.F',
                'get_btflux.F',
                'get_bulk.F',
                'get_grid.F',
                'get_initial.F',
                'get_initial_floats.F',
                'get_psource.F',
                'get_psource_ts.F',
                'get_smflux.F',
                'get_srflux.F',
                'get_ssh.F',
                'get_sss.F',
                'get_sst.F',
                'get_stflux.F',
                'get_tclima.F',
                'get_tides.F',
                'get_uclima.F',
                'get_wwave.F',
                'grid_stiffness.F',
                'init_floats.F',
                'init_sta.F',
                'insert_node.F',
                'lenstr.F',
                'main.F',
                'mas.F',
                'module_oa_interface.F90',
                'mpc.F',
                'ncjoin.F',
                'ncrename.F',
                'obc_volcons.F',
                'online_get_bulk.F',
                'online_interp.F',
                'online_set_bulk.F',
                'partit.F',
                'read_inp.F',
                'sediment.F',
                'sed_MUSTANG_CROCO.F90',
                'sed_MUSTANG.F90',
                'sedrst.F90',
                'sedwri.F90',
                'set_cycle.F',
                'setup_grid2.F',
                'setup_kwds.F',
                'srcscheck.F',
                'substance.F90',
                'wrt_avg.F',
                'wrt_bio_diags_avg.F',
                'wrt_bio_diags.F',
                'wrt_diags_avg.F',
                'wrt_diags_eddy_avg.F',
                'wrt_diags_ek_avg.F',
                'wrt_diags_ek.F',
                'wrt_diags.F',
                'wrt_diagsM_avg.F',
                'wrt_diagsM.F',
                'wrt_diags_pv_avg.F',
                'wrt_diags_pv.F',
                'wrt_diags_vrt_avg.F',
                'wrt_diags_vrt.F',
                'wrt_floats.F',
                'wrt_grid.F',
                'wrt_his.F',
                'wrt_rst.F',
                'wrt_sta.F',
                'wrt_surf_avg.F',
                'wrt_surf.F',
                'zoom.F',
            ]
            
            # Problem with psyir frontend parser
            excluded_files += [
                'wetdry.F',
                'gls_mixing.F',
                'get_date.F',
                'online_bulk_var.F',
                'online_interpolate_bulk.F'
            ]
        
        excluded_files = [i.replace(".F", ".f_pass1") for i in excluded_files]
        
        """
        Fall back to fparser to at least convert the file
        """ 
        if input_filename in excluded_files:
            mode = "fparser2"
    
    else:
        excluded_files = []





if mode == "copy":
    """
    Simple input/output pipeline to test the source-to-source processing
    """

    if args.verbosity >= 10:
        print(f"Reading from file '{args.source_file}'")

    f = open(args.source_file, "br")

    data = f.read()

    if args.verbosity >= 10:
        print(f"Writing to file '{args.output_file}'")

    f = open(args.output_file, "bw")
    f.write(data)

    sys.exit(0)


elif mode == "fparser2":
    """
    Direct fparser utilization
    
    We don't use this, since
    * the representation in psyir is much better
    * additional checks are done in psyir (such as if some variables exist)
    """
    
    from fparser.common.readfortran import FortranFileReader
    from fparser.two.utils import walk
    from fparser.two.parser import ParserFactory

    reader = FortranFileReader(
                args.source_file,
                ignore_comments=True
            )
    
    f2008_parser = ParserFactory().create(std="f2003")
    parse_tree = f2008_parser(reader)
    
    #f = open(args.debug_output_file, "w")
    #f.write(str(parse_tree))
    
    def print_subroutines(node):
        for node in walk(parse_tree, Subroutine_Stmt):
            print(node.get_name())
    
    def rec_follow(node, matching_class):
        for node_ in node.children:
            print(node_)
            if isinstance(node_, matching_class):
                print(node_.get_name())
            else:
                rec_follow(node_, matching_class)

    #print(parse_tree)

    f = open(args.output_file, "wb") 
    if fixedform_output:
        f.write(parse_tree.tofortran(tab="      ", isfix=True).encode())
    else:
        f.write(parse_tree.tofortran(isfix=False).encode())
    
    if args.verbosity >= 10:
        sys.stderr.write("Warning: Resulting file require to compile using gfortran with -ffixed-line-length-none\n")

    sys.exit(0)


elif mode == "psyir":
    from psyclone.psyir.frontend.fortran import FortranReader
    from psyclone.psyir.backend.fortran import FortranWriter
    
    reader = FortranReader()
    psyir_tree = reader.psyir_from_file(args.source_file)
    
    writer = FortranWriter()
    result = writer(psyir_tree)
    
    # fixed form indentation for "old" Fortran files
    if fixedform_output:
        lines = result.split("\n")

        import re
        for i in range(len(lines)):
            line = lines[i]
            m = re.match('^([     ]*)([0123456789]+)([     ]*)(.*)$', line)

            if m:
                prefix = m.group(1)
                label = m.group(2)
                mid = m.group(3)
                code = m.group(4)

                lines[i] = label + " "*(6-len(label)+len(prefix)+len(mid)) + code
            else:
                lines[i] = " "*6+lines[i]

        result = "\n".join(lines)
    
    f = open(args.output_file, "wb")
    f.write(result.encode())
    
    sys.exit(0)
    
    print("")
    print("Subroutines:")
    print_subroutines(parse_tree)
    
    print("")
    print("Variables")
    
    
    rec_follow(parse_tree, Subroutine_Stmt)
    
    if 0:
        for node in parse_tree.children:
            print("X")
            print(node.tostr())
    
    sys.exit(0)
    
else:
    raise Exception("TODO")
    
