#!/bin/bash

source job_config.inc.sh || exit 1
source "${OCEAN_SOURCE_DIR}/job_helpers/job_lib.inc.sh" || exit 1


###############################################################################
croco_echo "Changing to compile directory '${COMPILE_DIR}'"
###############################################################################
cd "${COMPILE_DIR}"
croco_echo_indent

###############################################################################
croco_echo "Making dependencies..."
###############################################################################
croco_echo $MAKE depend
$MAKE depend $@ || exit 1

###############################################################################
croco_echo "Making everything..."
###############################################################################
croco_echo $MAKE -j $@
$MAKE -j $@ || exit 1

###############################################################################
croco_echo "Finishing..."
###############################################################################

test -f croco  && mv croco $RUN_DIR
test -f partit && mv partit $RUN_DIR
test -f ncjoin && mv ncjoin  $RUN_DIR

###############################################################################
croco_echo "CROCO is ready to be used"
###############################################################################

croco_echo_unindent
