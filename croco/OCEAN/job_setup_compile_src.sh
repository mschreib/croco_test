#!/bin/bash

# Activate stopping on first unhandled error
set -e

source job_config.inc.sh || exit 1
source "${OCEAN_SOURCE_DIR}/job_helpers/job_lib.inc.sh" || exit 1

croco_echo "$0 START"

croco_echo_indent

###############################################################################
# Output CROCO config
###############################################################################
croco_print_config
croco_echo_nl


###############################################################################
# Make and get real path
###############################################################################
mkdir -p "$COMPILE_DIR" || exit 1
COMPILE_DIR="$(cd "$COMPILE_DIR"; pwd)"


###############################################################################
croco_echo "Setting up CROCO compile source directory: '$COMPILE_DIR'..."
###############################################################################

croco_echo_indent

croco_echo "Cleaning up..."

if true; then
	croco_echo_indent
	#
	# clean scratch area
	#

	croco_echo "removing '$COMPILE_DIR'"
	rm -rf "$COMPILE_DIR" || exit 1

	croco_echo "mkdir '$COMPILE_DIR'"
	mkdir -p "$COMPILE_DIR" || exit 1

	croco_echo_unindent
fi


###############################################################################
croco_echo "Setup compiler '${CROCO_COMPILER}'"
###############################################################################
croco_echo_indent

COMPILE_SUBDIRS=""
COMPILE_SUBDIRS+=" main ts_2d ts_3d nonhydro seawater_eos vertical_mixing model_diagnostics"
COMPILE_SUBDIRS+=" netcdf main_forcing wave_forcing surface_forcing floats station_diagnostics bio_sediments mpi AGRIF oasis"
COMPILE_SUBDIRS+=" bin"
COMPILE_SUBDIRS+=" lib"
COMPILE_SUBDIRS+=" include"

#COMPILE_SUBDIRS+=" pisces"


setup_source_dst "${OCEAN_SOURCE_DIR}/job_helpers" "${RUN_DIR}"

MAKE_COMPILER_FILE="${RUN_DIR}/job_helpers/${CROCO_COMPILER}.inc.sh"

if [ ! -e "$MAKE_COMPILER_FILE" ]; then
	croco_echo_error "The file '$MAKE_COMPILER_FILE' doesn't exist"
	exit 1
fi

source "$MAKE_COMPILER_FILE"

croco_echo_unindent


###############################################################################
croco_echo "Setup sources from '${OCEAN_SOURCE_DIR}/*'"
###############################################################################
croco_echo_indent

for i in ${COMPILE_SUBDIRS}; do
	setup_source_dst ${OCEAN_SOURCE_DIR}/${i}/* "${COMPILE_DIR}/${i}"
done

setup_source ${OCEAN_SOURCE_DIR}/Make*
setup_source ${OCEAN_SOURCE_DIR}/gendeps.py
setup_source ${OCEAN_SOURCE_DIR}/*.F
setup_source ${OCEAN_SOURCE_DIR}/*.F90
setup_source ${OCEAN_SOURCE_DIR}/*.h
setup_source ${OCEAN_SOURCE_DIR}/amr.in
setup_source ${ROOT_DIR}/XIOS/*.F

# MUSTANG
setup_source_dst ${ROOT_DIR}/MUSTANG/* "${COMPILE_DIR}/MUSTANG/"

# PISCES
setup_source_dst ${ROOT_DIR}/PISCES/* "${COMPILE_DIR}/PISCES"
setup_source_dst ${ROOT_DIR}/PISCES/SED/* "${COMPILE_DIR}/PISCES"
setup_source_dst ${ROOT_DIR}/PISCES/kRGB61* "${COMPILE_DIR}/PISCES"

croco_echo_unindent


###############################################################################
croco_echo "Pisces:"
###############################################################################
croco_echo_indent
if [[ -e "${RUN_DIR}/namelist_pisces_ref" ]] ; then
        croco_echo "File namelist_pisces exists in Run directory"
else
        croco_echo "Setup file namelist_pisces from source directory"
	setup_source_dst ${ROOT_DIR}/PISCES/namelist_pisces* "${RUN_DIR}" || exit 1
fi
croco_echo_unindent


###############################################################################
croco_echo "Mustang:"
###############################################################################
croco_echo_indent
if [[ -d "$RUN_DIR/MUSTANG_NAMELIST" ]]; then
        croco_echo "Mustang namelist directory MUSTANG_NAMELIST exists"
else
        croco_echo "Setup file para*txt from source directory"
        mkdir -p "$RUN_DIR/MUSTANG_NAMELIST"
        setup_source_dst ${ROOT_DIR}/MUSTANG/MUSTANG_NAMELIST/*txt "$RUN_DIR/MUSTANG_NAMELIST/" || exit 1
fi
croco_echo_unindent


###############################################################################
croco_echo "Setup local files from '${LOCAL_SOURCE_DIR}/*':"
###############################################################################
croco_echo_indent

if [ ! -d "${LOCAL_SOURCE_DIR}" ]; then
	croco_echo "Local source directory doesn't exist (no problem if you don't write your own code)"
else
	croco_echo "Setting up files in './src/' for compile directory"
	cd "${LOCAL_SOURCE_DIR}"

	croco_echo_indent
	for F in `find . -type f`; do
		croco_echo "${F}"
		rm -f "${COMPILE_DIR}/$F"
		cp "$F" "${COMPILE_DIR}/$F"
	done
	croco_echo_unindent

	cd "${RUN_DIR}"
fi

croco_echo_unindent



###############################################################################
croco_echo "Changing to compile directory for further tests"
###############################################################################
croco_echo_indent

cd "${COMPILE_DIR}"

CPPFLAGS1="${CPPFLAGS1} ${NETCDFINC} -I./include"

###############################################################################
croco_echo_nonl "Checking COMPILEAGRIF... "
###############################################################################
unset COMPILEAGRIF
if $($CPP1 $CPPFLAGS1 testkeys.F | grep -i -q agrifisdefined) ; then
	croco_echo_color_green
	croco_echo_append_nl "activated"
	croco_echo_color_reset
	croco_echo_indent
	COMPILEAGRIF=TRUE
	FFLAGS1="${FFLAGS1} -IAGRIF"
	F90FLAGS1="${F90FLAGS1} -IAGRIF"
	LDFLAGS1="-LAGRIF -lagrif $LDFLAGS1"

	CPPFLAGS1="${CPPFLAGS1} -ICROCOFILES/AGRIF_INC"
	# we use the AGRIF Makedefs.generic definition
	cp -a -f Makedefs.generic.AGRIF Makedefs.generic
	croco_echo_unindent
else
	croco_echo_color_yellow
	croco_echo_append_nl "not activated"
	croco_echo_color_reset
fi

###############################################################################
croco_echo_nonl "Checking COMPILEMPI... "
###############################################################################
unset COMPILEMPI
if $($CPP1 $CPPFLAGS1 testkeys.F | grep -i -q mpiisdefined) ; then
	croco_echo_color_green
	croco_echo_append_nl "activated"
	croco_echo_color_reset
	croco_echo_indent
	COMPILEMPI=TRUE
	LDFLAGS1="${LDFLAGS1} ${MPILIB}"
	CPPFLAGS1="${CPPFLAGS1} ${MPIINC}"
	FFLAGS1="${FFLAGS1} ${MPIINC}"
	F90FLAGS1="${F90FLAGS1} ${MPIINC}"
	CFT1="${MPIF90}"
	croco_echo_unindent
else
	croco_echo_color_yellow
	croco_echo_append_nl "not activated"
	croco_echo_color_reset
fi

###############################################################################
# Take environment variables for compiler and options
###############################################################################
FFLAGS1=${CROCO_FFLAGS1-$FFLAGS1}
F90FLAGS1=${CROCO_F90FLAGS1-$F90FLAGS1}
CFT1=${CROCO_CFT1-$CFT1}


###############################################################################
croco_echo_nonl "Checking COMPILEXIOS... "
###############################################################################
#
# - Determine if XIOS librairies is required 
# - if it is the case :
#     => if XIOS compiled with oasis, add the OASIS inc. files and librairies
#     => pre-processing (using cpp) of the xml files required by XIOS 
#
unset COMPILEXIOS
XIOS_ROOT_DIR=${CROCO_XIOS_ROOT_DIR-$XIOS_ROOT_DIR}
if $($CPP1 $CPPFLAGS1 testkeys.F | grep -i -q xiosisdefined) ; then
	croco_echo_color_green
	croco_echo_append_nl "activated"
	croco_echo_color_reset
	croco_echo_indent
        COMPILEXIOS=TRUE
        LDFLAGS1="$LDFLAGS1 $XIOS_ROOT_DIR/lib/libxios.a  -lstdc++ -lnetcdff -lnetcdf"
        CPPFLAGS1="$CPPFLAGS1 -I$XIOS_ROOT_DIR/inc"
        FFLAGS1="$FFLAGS1 -I$XIOS_ROOT_DIR/inc"
        F90FLAGS1="$F90FLAGS1 -I$XIOS_ROOT_DIR/inc"
	
        ln -fs $XIOS_ROOT_DIR/bin/xios_server.exe $RUN_DIR/.
	croco_echo_unindent
else
	croco_echo_color_yellow
	croco_echo_append_nl "not activated"
	croco_echo_color_reset
fi

###############################################################################
croco_echo_nonl "Checking COMPILEOASIS... "
###############################################################################
unset COMPILEOASIS
PRISM_ROOT_DIR=${CROCO_PRISM_ROOT_DIR-$PRISM_ROOT_DIR}
if $($CPP1 $CPPFLAGS1 testkeys.F | grep -i -q oacplisdefined) ; then
	croco_echo_color_green
	croco_echo_append_nl "activated"
	croco_echo_color_reset
	croco_echo_indent
	CHAN=MPI1
	LIBPSMILE="${PRISM_ROOT_DIR}/lib/libpsmile.${CHAN}.a \
		${PRISM_ROOT_DIR}/lib/libmct.a  \
		${PRISM_ROOT_DIR}/lib/libmpeu.a \
		${PRISM_ROOT_DIR}/lib/libscrip.a"
	PSMILE_INCDIR="-I${PRISM_ROOT_DIR}/build/lib/psmile.${CHAN} \
		-I${PRISM_ROOT_DIR}/build/lib/mct"
	COMPILEOASIS=TRUE
	LDFLAGS1="$LDFLAGS1 $LIBPSMILE $NETCDFLIB"
	CPPFLAGS1="$CPPFLAGS1 ${PSMILE_INCDIR} $NETCDFINC"
	FFLAGS1="$FFLAGS1 ${PSMILE_INCDIR} $NETCDFINC"
	F90FLAGS1="$F90FLAGS1 ${PSMILE_INCDIR} $NETCDFINC"
	croco_echo_unindent
else
	croco_echo_color_yellow
	croco_echo_append_nl "not activated"
	croco_echo_color_reset
fi


###############################################################################
# prepare and compile the AGRIF library
###############################################################################
if [[ $COMPILEAGRIF ]] ; then
	croco_echo "Processing COMPILEAGRIF... "
	croco_echo_indent

	#
	# compile the AGRIF librairy
	#
	if [[ $COMPILEMPI ]] ; then
		$MAKE -C AGRIF FC="$CFT1" CPP="$CPP1" CPPFLAGS="-DAGRIF_MPI $MPIINC" FFLAGS="$FFLAGS1" F90FLAGS="$F90FLAGS1"
	else
		$MAKE -C AGRIF FC="$CFT1" CPP="$CPP1" FFLAGS="$FFLAGS1" F90FLAGS="$F90FLAGS1"
	fi
	if [[ $OS == Darwin ]] ; then          # DARWIN
		# run RANLIB on Darwin system
		ranlib AGRIF/libagrif.a
	fi

	mkdir CROCOFILES
	mkdir -p CROCOFILES/AGRIF_MODELFILES
	mkdir -p CROCOFILES/AGRIF_INC
	$CPP1 amr.in | grep -v -e ! -e '#' -e % -e '*' > CROCOFILES/amr.scrum
	mv AGRIF/conv CROCOFILES/.
	for i in *.h *.h90 ; do
		croco_echo $i
		cat cppdefs.h $i | cpp -P | grep -v -e ! -e '#' -e % -e '*' > CROCOFILES/$i
	done
	mv -f CROCOFILES/private_scratch_AMR.h CROCOFILES/private_scratch.h
	croco_echo_unindent
fi

###############################################################################
croco_echo_nonl "Checking COMPILEOMP... "
###############################################################################
# determine if OPENMP compilation is needed
unset COMPILEOMP
if $($CPP1 $CPPFLAGS1 testkeys.F | grep -i -q openmp) ; then
	croco_echo_color_red
	croco_echo_append_nl "activated"
	croco_echo_color_reset
	croco_echo_indent
	COMPILEOMP=TRUE
	if [[ $OS == Linux || $OS == Darwin ]] ; then 
		if [[ $FC == gfortran ]] ; then
			FFLAGS1="$FFLAGS1 -fopenmp"
			F90FLAGS1="$F90FLAGS1 -fopenmp"
		elif [[ $FC == ifort || $FC == ifc ]] ; then
			FFLAGS1="$FFLAGS1 -openmp"
			F90FLAGS1="$F90FLAGS1 -openmp"
		else
			FFLAGS1="$FFLAGS1 -openmp"
			F90FLAGS1="$F90FLAGS1 -openmp"
		fi
	elif [[ $OS == CYGWIN_NT-10.0 ]] ; then
        FFLAGS1=="$FFLAGS1 -fopenmp"
        F90FLAGS1=="$F90FLAGS1 -fopenmp"
	elif [[ $OS == AIX ]] ; then
		FFLAGS1="$FFLAGS1 -qsmp=omp"
		F90FLAGS1="$F90FLAGS1 -qsmp=omp"
		CFT1="xlf95_r"
	fi
	croco_echo_unindent
else
	croco_echo_color_yellow
	croco_echo_append_nl "not activated"
	croco_echo_color_reset
fi

###############################################################################
# Change back to run directory
###############################################################################
cd "${RUN_DIR}"

croco_echo_unindent
croco_echo_nl


###############################################################################
croco_echo "Rewriting '${COMPILE_DIR}/Makedefs'"
###############################################################################

rm -f "${COMPILE_DIR}/Makedefs"
echo 's?$(FFLAGS1)?'$FFLAGS1'?g' > "${COMPILE_DIR}/flags.tmp"
echo 's?$(F90FLAGS1)?'$F90FLAGS1'?g' >> "${COMPILE_DIR}/flags.tmp"
echo 's?$(LDFLAGS1)?'$LDFLAGS1'?g' >> "${COMPILE_DIR}/flags.tmp"
echo 's?$(CPP1)?'$CPP1'?g' >> "${COMPILE_DIR}/flags.tmp"
echo 's?$(CFT1)?'$CFT1'?g' >> "${COMPILE_DIR}/flags.tmp"
echo 's?$(CPPFLAGS1)?'$CPPFLAGS1'?g' >> "${COMPILE_DIR}/flags.tmp"
echo 's?$(USE_POSEIDON)?'$USE_POSEIDON'?g' >> "${COMPILE_DIR}/flags.tmp"

sed -f "${COMPILE_DIR}/flags.tmp" "${COMPILE_DIR}/Makedefs.generic" > "${COMPILE_DIR}/Makedefs"
rm -f "${COMPILE_DIR}/flags.tmp"

croco_echo_unindent

croco_echo_nl
croco_echo "Horray! Source for compilation now assembled in '${COMPILE_DIR}'"
croco_echo "You may now run ./job_compile.sh to compile CROCO"
croco_echo_nl

croco_echo_unindent

croco_echo "$0 END"
