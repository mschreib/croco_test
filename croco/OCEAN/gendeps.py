#! /usr/bin/env python3

import sys
import os
import re

from python_file_parser import *


class gendeps():
    """
    This class is a convenience handler to generate Makefile dependencies from Fortran source code
    """
    def __init__(self, verbosity = 10):
        self.verbosity = verbosity
        self.ffiles = FortranFiles(verbosity)

    def parse(self, files, mode=FortranFile.ALL):
        self.ffiles.parse(files, parseMode=mode)


    def _track_dependencies(self, fortranFile: FortranFile, depth = 0, max_depth=10, outputPrefix=""):
        """
        Return list of include file dependencies to which this file is dependent to
         * Includes are recursively tracked down
        """

        if depth >= max_depth:
            return []

        inc_deps = fortranFile.depsIncludeFiles.copy()

        # Recursively track down dependencies
        for depfile in fortranFile.depsIncludeFiles:
            try:
                depfilepath = self.ffiles.basename_to_fortranFiles[depfile]
                if self.verbosity >= 10:
                    print(outputPrefix+f" - dependency: {depfile}")
            except KeyError:
                print(f"WARNING: Unable to resolv dependency to '{depfile}'")
                continue

            inc_deps += self._track_dependencies(depfilepath, depth+1, max_depth, outputPrefix=outputPrefix+"  ")

        """
        Work through module dependencies
        """
        mod_deps = []
        for module in fortranFile.depsUseModule:
            if module in self.ffiles.module_to_fullpath:
                modfullpath = self.ffiles.module_to_fullpath[module]
                # Make sure that module is build before building this file
                mod_deps += [self.ffiles.getFortranFile(modfullpath).objFilepath]

            else:
                if self.verbosity > 0:
                    print(f"WARNING: Module '{module}' not found! Unable to create a dependency")

        deps = inc_deps + mod_deps

        # Remove duplicates
        deps = list(dict.fromkeys(deps))
        return deps


    def get_makefile_dependencies(self):
        """
        Return Makefile dependencies for all files
        """
        retval = ""

        if self.verbosity >= 10:
            print("Generating depencendies:")
            
        #for filepath in self.ffiles.filesDict:
        for fortranFile in self.ffiles.fortranFiles:

            if self.verbosity >= 10:
                print(f" - File '{fortranFile.filePath}")

            """
            Check if this file will be compiled. If not, skip it
            """
            if fortranFile.objFilepath is None:
                if self.verbosity >= 10:
                    print("   - none (target is no object file)")
                continue

            retval += f"{fortranFile.objFilepath}:"

            if self.verbosity >= 10:
                print(f"   - target: {fortranFile.objFilepath}")

            file_deps = self._track_dependencies(fortranFile, outputPrefix=" "*2)

            # Remove circular dependency
            if fortranFile.objFilepath in file_deps:
                print(f"WARNING: Circular dependency detected for '{fortranFile.objFilepath}'")
                file_deps.remove(fortranFile.objFilepath)

            """
            Create dependency list
            """
            for dfile in file_deps:
                basename = os.path.basename(dfile)
                if basename in self.ffiles.basename_to_fortranFiles:
                    retval += " "+self.ffiles.basename_to_fortranFiles[basename].filePath
                elif dfile.endswith(".o"):
                    retval += " "+dfile
                else:
                    if basename not in ['netcdf.inc']:
                        if self.verbosity > 0:
                            print(f"WARNING: File '{basename}' not found")

            retval += "\n"

        return retval


def print_help():
    print("""

This script generates Makefile dependencies from Fortran source files.

Usage:

"""+sys.argv[0]+""" [program options] [file1] [file2] [...]

[file1/2/3] can be Fortran files (.f90, .f, etc.) as well as header files (.fhh, .h)

Options:
    -v [int]    Specify verbosity level (0=no output, 100=output all)
    -o [str]    Specify output file (e.g., "makefile.inc") to write dependencies to

If no output file is given, the output is written to stdout
""")


if len(sys.argv) <= 1:
    print_help()
    sys.exit(1)


files = []
output_file = None
verbosity = 0


"""
Poor man's argument parser
"""

# Type of argument we are parsing. Default is 'file'
state = "file"
for a in sys.argv[1:]:
    if state == "output":
        output_file = a
        state = "file"
        continue

    if state == "verbose":
        verbosity = int(a)
        state = "file"
        continue

    if a == '-o':
        state = "output"
        continue

    if a == '-v':
        state = "verbose"
        continue

    if a == '-h':
        print_help()
        sys.exit(0)

    if a[0] == "-":
        raise Exception(f"Unhandled argument '{a}'.")

    if state == "file":
        files.append(a)
        continue

    raise Exception("Internal parser error.")

if state != "file":
    raise Exception("Invalid arguments provided.")

if verbosity >= 100:
    print("Files:", files)


g = gendeps(verbosity)
g.parse(files, mode=FortranFile.INCLUDE | FortranFile.MODULE)

c = g.get_makefile_dependencies()

if output_file != None:
    f = open(output_file, "w")
    f.write(c)

else:
    print("Make dependencies:")
    print(c, end="")

